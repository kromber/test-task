var app = angular.module('testTask', ['ngRoute','ngMaterial']);

// Задаю конфигурацию приложения.
app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: false
	});

	// Указываю роуты приложения.
	$routeProvider
		.when('/', {
			templateUrl: '/pages/users.html',
			controller: 'UsersCtrl'
		})
		.when('/users/:login', {
			templateUrl: '/pages/user.html',
			controller: 'UserCtrl'
		})
		.when('/repo', {
			templateUrl: '/pages/repo.html',
			controller: 'RepoCtrl'
		})
		.when('/commits', {
			templateUrl: '/pages/commits.html',
			controller: 'CommitsCtrl'
		})
		.when('/page-not-found', {
			templateUrl: '/pages/404.html',
			controller: '404Ctrl'
		})
		.otherwise('/page-not-found', {
			redirectTo: '/page-not-found',
			controller: 'MainCtrl'
		});
}]);

// Основной контроллер. Служит как layOut.
app.controller('MainCtrl', function($scope, $location, $routeParams, $http, $rootScope, $window, $timeout) {
	// Данные о странице.
	$rootScope.page = {
	  // Название страницы.
	  name: 'users',
	  // URL страницы.
	  url: $location.url(),
	  // Тайтл страницы по умолчанию.
	  title: 'GitHub informer'
	};
	
	// Функция "Переход назад".
	$rootScope.back = function(){
		//Скрываю слой.
		$rootScope.move('hide');
		
		// Перехожу назад.
		window.history.back();
	}
	
	// Функция делает первую букву большой.
	$rootScope.bigFL = function (str){ return str[0].toUpperCase() + str.slice(1); };
	
	// Получение данных с сервера.
	$rootScope.getData = function(url, key, downloading){
		// Получаю данные.
	  $http.get(url).
		  //В случае успеха.
		  success(function(data){
		  	console.log(data);
		  	// Записываю данные в скоуп.
		  	$scope[key] = data;
		  	
		  	// Если получаю параметр о том, чтобы обновить параметр "Идет загрузка".
		  	if(typeof(downloading) === 'boolean'){
		  		// Устанавливаю значение парамета "Загрузка".
		  		$rootScope.downloading = downloading;
		  		
		  		// Если загрузка завершена.
		  		if(downloading === false){
		  			//Показываю слой.
					$rootScope.move('show');
		  		}
		  	}
		  }).
		  error(function(err){
		  	console.log(err);
		  	// Перенаправляю на страницу "404".
		  	$location.path('/page-not-found');
		  });
	};
	
	// Функция выдвежного вида.
	$rootScope.move = function(flag){
		if(flag === 'hide'){ // Если нужно скрыть.
			jQuery('.move').css({display: 'none'});
		
			// Идет загрузка.
			$rootScope.downloading = true;
			
			// Скрываю вид.
			jQuery('.move').css({left: '100%'});
		}else{ // Если нужно показать.
			// Загрузка завершена.
			$rootScope.downloading = false;
			
			jQuery('.move').css({display: 'block'});
			
			setTimeout(function(){
				// Показываю вид.
				jQuery('.move').css({left: '0px'});
			},200);
		}
	};
	
	//Скрываю слой.
	$rootScope.move('hide');
});

// Контроллер для страницы "Пользователи" (корневой URL).
app.controller('UsersCtrl', function($scope, $location, $routeParams, $http, $rootScope) {
	//Скрываю слой.
	$rootScope.move('hide');
	
	// Идет загрузка.
	$rootScope.downloading = true;
	
	// Данные о странице.
  $rootScope.page = {
  	// Название страницы.
  	name: 'users',
  	// URL страницы.
  	url: $location.url(),
  	// Тайтл страницы по умолчанию.
  	title: 'Все пользователи'
  };
	 
	// Получаю всех пользователей.
	$rootScope.getData('https://api.github.com/users', 'users', false);
});

// Контроллер для страницы "Информация о пользователе" (URL = "/user/{{user.login}}").
app.controller('UserCtrl', function($scope, $location, $routeParams, $http, $rootScope) {
	//Скрываю слой.
	$rootScope.move('hide');
	
	// Идет загрузка.
	$rootScope.downloading = true;
  
  // Данные о странице.
  $rootScope.page = {
  	// Название страницы.
  	name: 'user',
  	// URL страницы.
  	url: $location.url(),
  	// Тайтл страницы по умолчанию.
  	title: 'Логин: ' + $rootScope.bigFL($routeParams.login)
  };
  
  // Получаю данные пользователя.
  $http.get('https://api.github.com/users/' + $routeParams.login).
	  //В случае успеха.
	  success(function(data){
	  	// Получил пользователя.
	  	$scope.user = data;
	  	
	  	// Получаю список репозиториев.
			$rootScope.getData($scope.user.repos_url, 'repos', false);
	  }).
	  error(function(err){
	  	// Перенаправляю на страницу "404".
		  $location.path('/page-not-found');
		  	
	  	console.log(err);
	  	
	  });
});

// Контроллер для страницы "Репозиторий" (URL = "/users/:login/repo?name={{repo.name}}").
app.controller('RepoCtrl', function($scope, $location, $routeParams, $http, $rootScope) {
	//Скрываю слой.
	$rootScope.move('hide');
	
	// Идет загрузка.
	$rootScope.downloading = true;
	
	// Данные о странице.
	$rootScope.page = {
  	// Название страницы.
  	name: 'repo',
  	// URL страницы.
  	url: $location.url(),
  	// Тайтл страницы по умолчанию.
  	title: 'Репозиторий: "' + $rootScope.bigFL($routeParams.name) + '"'
  };
  
	// Из url получаю имя автора.
	$scope.username = $routeParams.username;
  
	// Логин пользователя.
	$scope.userlogin = $routeParams.login;
  
	// Название репо.
	$scope.reponame = $routeParams.name;
	  
	// Получаю данные репозитория.
	//$rootScope.getData('https://api.github.com/repos/'+ $scope.userlogin + '/' + $scope.reponame, 'repo', false);
	
	// Получаю данные веток.
	$rootScope.getData('https://api.github.com/repos/' + $scope.userlogin + '/' + $scope.reponame + '/branches', 'branches', false);
	
	// Перехожу на страницу со списком коммитов.
	$scope.getRepo = function(){
		var url = '/commits?userlogin=' + $scope.userlogin+ '&reponame=' + $scope.reponame;
		
		$location.url(url);
	};
});

// Контроллер для страницы "Список комметов" (URL = "/commits?userlogin={{userlogin}}&reponame={{reponame}}").
app.controller('CommitsCtrl', function($scope, $location, $routeParams, $http, $rootScope) {
	//Скрываю слой.
	$rootScope.move('hide');
	
	// Идет загрузка.
	$rootScope.downloading = true;
	
	// Данные о странице.
  $rootScope.page = {
  	// Название страницы.
  	name: 'commits',
  	// URL страницы.
  	url: $location.url(),
  	// Тайтл страницы по умолчанию.
  	title: 'Коммиты репо: "' + $rootScope.bigFL($routeParams.reponame) + '"'
  };
  
  // Из url получаю название рапо.
  $scope.reponame = $routeParams.reponame;
  
  // Логин пользователя.
  $scope.userlogin = $routeParams.userlogin;
  
  // Получаю список веток.
	$rootScope.getData('https://api.github.com/repos/' + $scope.userlogin + '/' + $scope.reponame + '/commits', 'commits', false);
});

// Контроллер для страницы "404".
app.controller('404Ctrl', function($scope, $location, $routeParams, $http, $rootScope) {
	//Скрываю слой.
	$rootScope.move('hide');
	
	// Идет загрузка.
	$rootScope.downloading = true;
	
	// Данные о странице.
  $rootScope.page = {
  	// Название страницы.
  	name: '404',
  	// URL страницы.
  	url: $location.url(),
  	// Тайтл страницы по умолчанию.
  	title: 'Страница не найдена'
  };
  
  // Страница загружена.
	$rootScope.downloading = false;
	
	//Показываю слой.
	$rootScope.move('show');
});